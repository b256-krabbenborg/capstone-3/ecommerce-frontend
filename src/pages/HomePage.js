import { useEffect, useReducer } from 'react';
import axios from 'axios';
import { Row, Col } from 'react-bootstrap';
import Product from '../components/Product';
import { Helmet } from 'react-helmet';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';

axios.defaults.baseURL = process.env.REACT_APP_API_URL;

const reducer = (state, action) => {
  switch (action.type) {
    case 'FETCH_REQUEST':
      return { ...state, loading: true };
    case 'FETCH_SUCCESS':
      return { ...state, products: action.payload, loading: false };
    case 'FETCH_FAIL':
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};

function HomePage() {
  const [{ loading, error, products }, dispatch] = useReducer(reducer, {
    products: [],
    loading: true,
    error: '',
  });

  useEffect(() => {
    const fetchData = async () => {
      dispatch({ type: 'FETCH_REQUEST' });
      try {
        const result = await axios.get(
          `${process.env.REACT_APP_API_URL}/products`
        );
        dispatch({ type: 'FETCH_SUCCESS', payload: result.data });
      } catch (err) {
        dispatch({ type: 'FETCH_FAIL', payload: err.message });
      }
    };
    fetchData();
  }, []);

  return (
    <div>
      <Helmet>
        <title>SKINKR</title>
      </Helmet>
      <div
        className="hero-image"
        style={{
          backgroundImage: `url('../images/carousel/banner1.jpg')`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          height: '100vh', // Adjust the height to exclude the header height
          width: '100vw', // Set the width to occupy the full viewport width
        }}
      >
        <div className="hero-content">
          <h1>Welcome to SKINKR</h1>
          <p>Discover the best skincare products.</p>
        </div>
      </div>
    </div>
  );
}

export default HomePage;
