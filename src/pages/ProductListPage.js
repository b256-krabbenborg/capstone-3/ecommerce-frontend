import React, { useContext, useEffect, useReducer } from 'react';
import axios from 'axios';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';
import { Store } from '../Store';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import { getError } from '../utils';

axios.defaults.baseURL = process.env.REACT_APP_API_URL;

const reducer = (state, action) => {
  switch (action.type) {
    case 'FETCH_REQUEST':
      return { ...state, loading: true };
    case 'FETCH_SUCCESS':
      return {
        ...state,
        products: action.payload.products,
        page: action.payload.page,
        pages: action.payload.pages,
        loading: false,
      };
    case 'FETCH_FAIL':
      return { ...state, loading: false, error: action.payload };
    case 'CREATE_REQUEST':
      return { ...state, loadingCreate: true };
    case 'CREATE_SUCCESS':
      return {
        ...state,
        loadingCreate: false,
      };
    case 'CREATE_FAIL':
      return { ...state, loadingCreate: false };

    case 'DELETE_REQUEST':
      return { ...state, loadingDelete: true, successDelete: false };
    case 'DELETE_SUCCESS':
      return {
        ...state,
        loadingDelete: false,
        successDelete: true,
      };
    case 'DELETE_FAIL':
      return { ...state, loadingDelete: false, successDelete: false };

    case 'DELETE_RESET':
      return { ...state, loadingDelete: false, successDelete: false };
    default:
      return state;
  }
};

export default function ProductListPage() {
  const [
    {
      loading,
      error,
      products,
      pages,
      loadingCreate,
      loadingDelete,
      successDelete,
    },
    dispatch,
  ] = useReducer(reducer, {
    loading: true,
    error: '',
  });

  const navigate = useNavigate();
  const { search } = useLocation();
  const sp = new URLSearchParams(search);
  const page = sp.get('page') || 1;

  const { state } = useContext(Store);
  const { userInfo } = state;

  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data } = await axios.get(`/products/admin?page=${page} `, {
          headers: { Authorization: `Bearer ${userInfo.token}` },
        });

        dispatch({ type: 'FETCH_SUCCESS', payload: data });
      } catch (err) {}
    };

    if (successDelete) {
      dispatch({ type: 'DELETE_RESET' });
    } else {
      fetchData();
    }
  }, [page, userInfo, successDelete]);

  const createHandler = async () => {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You are about to create a new product',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, create it!',
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          dispatch({ type: 'CREATE_REQUEST' });
          const { data } = await axios.post(
            '/products',
            {},
            {
              headers: { Authorization: `Bearer ${userInfo.token}` },
            }
          );
          Swal.fire('Created!', 'Product created successfully.', 'success');
          dispatch({ type: 'CREATE_SUCCESS' });
          navigate(`/admin/product/${data.product._id}`);
        } catch (err) {
          Swal.fire('Failed!', getError(err), 'error');
          dispatch({
            type: 'CREATE_FAIL',
          });
        }
      }
    });
  };

  const deleteHandler = async (product) => {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You are about to delete a product',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          await axios.delete(`/products/${product._id}`, {
            headers: { Authorization: `Bearer ${userInfo.token}` },
          });
          Swal.fire('Deleted!', 'Product deleted successfully.', 'success');
          dispatch({ type: 'DELETE_SUCCESS' });
        } catch (err) {
          Swal.fire('Failed!', getError(err), 'error');
          dispatch({
            type: 'DELETE_FAIL',
          });
        }
      }
    });
  };

  const deactivateHandler = async (product) => {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You are about to deactivate a product',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, deactivate it!',
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          await axios.put(
            `/products/deactivate/${product._id}`,
            {},
            {
              headers: { Authorization: `Bearer ${userInfo.token}` },
            }
          );
          Swal.fire(
            'Deactivated!',
            'Product deactivated successfully.',
            'success'
          );
          dispatch({ type: 'DEACTIVATE_SUCCESS' });
        } catch (err) {
          Swal.fire('Failed!', getError(err), 'error');
          dispatch({ type: 'DEACTIVATE_FAIL' });
        }
      }
    });
  };

  const reactivateHandler = async (product) => {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You are about to reactivate a product',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, reactivate it!',
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          await axios.put(
            `/products/reactivate/${product._id}`,
            {},
            {
              headers: { Authorization: `Bearer ${userInfo.token}` },
            }
          );
          Swal.fire(
            'Reactivated!',
            'Product reactivated successfully.',
            'success'
          );
          dispatch({ type: 'REACTIVATE_SUCCESS' });
        } catch (err) {
          Swal.fire('Failed!', getError(err), 'error');
          dispatch({ type: 'REACTIVATE_FAIL' });
        }
      }
    });
  };

  return (
    <div>
      <Row>
        <Col>
          <h1>Products</h1>
        </Col>
        <Col className="col text-end">
          <div>
            <Button type="button" onClick={createHandler}>
              Create Product
            </Button>
          </div>
        </Col>
      </Row>

      {loadingCreate && <LoadingBox></LoadingBox>}
      {loadingDelete && <LoadingBox></LoadingBox>}

      {loading ? (
        <LoadingBox></LoadingBox>
      ) : error ? (
        <MessageBox variant="danger">{error}</MessageBox>
      ) : (
        <>
          <table className="table">
            <thead>
              <tr>
                {/* <th>ID</th> */}
                <th>NAME</th>
                {/* <th>PRICE</th>
                <th>CATEGORY</th> */}
                <th>BRAND</th>
                <th className="text-center">ACTIONS</th>
              </tr>
            </thead>
            <tbody>
              {products.map((product) => (
                <tr key={product._id}>
                  {/* <td>{product._id}</td> */}
                  <td>{product.name}</td>
                  {/* <td>{product.price}</td>
                  <td>{product.category}</td> */}
                  <td>{product.brand}</td>
                  <td>
                    <div className="d-flex justify-content-center">
                      <Button
                        type="button"
                        variant="light"
                        onClick={() =>
                          navigate(`/admin/product/${product._id}`)
                        }
                      >
                        Edit
                      </Button>
                      &nbsp;
                      <Button
                        type="button"
                        variant="light"
                        onClick={() => deleteHandler(product)}
                      >
                        Delete
                      </Button>
                      &nbsp;
                      {product.isActive ? (
                        <button onClick={() => deactivateHandler(product)}>
                          Deactivate
                        </button>
                      ) : (
                        <button
                          style={{ backgroundColor: '#a40f5f', color: 'white' }}
                          onClick={() => reactivateHandler(product)}
                        >
                          Reactivate
                        </button>
                      )}
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <div>
            {[...Array(pages).keys()].map((x) => (
              <Link
                className={x + 1 === Number(page) ? 'btn text-bold' : 'btn'}
                key={x + 1}
                to={`/admin/products?page=${x + 1}`}
              >
                {x + 1}
              </Link>
            ))}
          </div>
        </>
      )}
    </div>
  );
}
