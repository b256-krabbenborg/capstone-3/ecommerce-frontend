const data = {
  products: [
    {
      name: 'AHA BHA PHA 30 Days Miracle Toner',
      slug: 'somebymi-aha-bha-pha',
      category: 'Toner',
      image: '/images/p1.jpg',
      price: 16.95,
      countInStock: 10,
      brand: 'Some By Mi',
      rating: 4.5,
      numReviews: 10,
      description:
        'A cult favorite, one bottle of this multitasking toner is sold every three seconds! As its name suggests, the formula boasts three types of chemical exfoliants (AHAs, BHAs, and PHAs) plus papaya and witch hazel extracts to effectively boost cell turnover and keep the skin smooth and healthy. A high concentration of the tea tree water extract also promotes clear skin and soothes inflammation, while adenosine and niacinamide target wrinkles and a dull skin tone.',
    },
    {
      name: 'Time Revolution The First Treatment Essence RX',
      slug: 'missha-time-revolution',
      category: 'Essence',
      image: '/images/p2.jpg',
      price: 34.95,
      countInStock: 10,
      brand: 'Missha',
      rating: 4.5,
      numReviews: 10,
      description:
        'The Missha Time Revolution The First Treatment Essence Rx is the 4th generation and updated version of the cult favorite essence among skincare enthusiasts, known for its effectiveness in delivering multiple benefits to the skin such as brightening, smoothing, hydrating, firming, wrinkle improvement, repairing damaged skin and evening out skin tone in just a few weeks.',
    },
    {
      name: 'Dynasty Cream',
      slug: 'joseon-dynasty-cream',
      category: 'Shirts',
      image: '/images/p3.jpg',
      price: 19.45,
      countInStock: 10,
      brand: 'Beauty of Joseon',
      rating: 4.5,
      numReviews: 10,
      description:
        'The Beauty of Joseon Dynasty Cream is a brightening antiaging cream made with natural hanbang (traditional Korean herbal medicine) ingredients for a healthier complexion. Revitalising ginseng and softening orchid help to firm, smooth, and moisturise the skin, while safflower seed oil and ceramide strengthen and protect the skin barrier',
    },
    {
      name: 'All Clean Balm',
      slug: 'heimish-all-clean-balm',
      category: 'Cleanser',
      image: '/images/p4.jpg',
      price: 16.95,
      countInStock: 10,
      brand: 'Heimish',
      rating: 4.5,
      numReviews: 10,
      description:
        'The Heimish All Clean Balm is a sherbet oil cleanser formulated with natural botanical ingredients for deep cleansing and removing makeup and sunscreen. It starts as a solid balm, and transforms into an oil cleanser on the skin, and becomes milky while rinsing.',
    },
  ],
};

export default data;
