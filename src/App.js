import { BrowserRouter, Route, Routes, Link } from 'react-router-dom';
// eslint-disable-next-line
import Swal from 'sweetalert2';
import HomePage from './pages/HomePage';
import ProductPage from './pages/ProductPage';
import {
  Navbar,
  Badge,
  Nav,
  NavDropdown,
  Container,
  Button,
  Row,
  Col,
} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { useContext, useEffect, useState } from 'react';
import { Store } from './Store';
import CartPage from './pages/CartPage';
import LoginPage from './pages/LoginPage';
import ShippingAddressPage from './pages/ShippingAddressPage';
import RegisterPage from './pages/RegisterPage';
import PaymentPage from './pages/PaymentPage';
import CheckoutPage from './pages/CheckoutPage';
import OrderPage from './pages/OrderPage';
import OrderHistoryPage from './pages/OrderHistoryPage';
import ProfilePage from './pages/ProfilePage';
import { getError } from './utils';
import axios from 'axios';
import SearchBox from './components/SearchBox';
import SearchPage from './pages/SearchPage';
import ProtectedRoute from './components/ProtectedRoute';
import AdminDashboardPage from './pages/AdminDashboardPage';
import AdminRoute from './components/AdminRoute';
import ProductListPage from './pages/ProductListPage';
import ProductEditPage from './pages/ProductEditPage';
import OrderListPage from './pages/OrderListPage';
import UserListPage from './pages/UserListPage';
import UserEditPage from './pages/UserEditPage';
import ProductsCataloguePage from './pages/ProductsCatalogue';

axios.defaults.baseURL = process.env.REACT_APP_API_URL;

function App() {
  const { state, dispatch: ctxDispatch } = useContext(Store);
  const { fullBox, cart, userInfo } = state;

  const logoutHandler = () => {
    ctxDispatch({ type: 'USER_LOGOUT' });
    localStorage.removeItem('userInfo');
    localStorage.removeItem('shippingAddress');
    localStorage.removeItem('paymentMethod');
    window.location.href = '/login';
  };

  return (
    <BrowserRouter>
      <div class="top-bar">
        <div class="container-fluid">
          <div class="top-bar-announcement">
            <div class="col-8 mr-auto">
              <p>ORDER BEFORE 3PM FOR SAME DAY SHIPPING MON-FRI</p>
            </div>
            <div class="col-4 ml-auto">
              <ul class="topbar-socials socials-list">
                <li>
                  <a href="https://www.facebook.com/shielalaland/">
                    <i class="fa-brands fa-square-facebook"></i>
                  </a>
                </li>
                <li>
                  <a href="https://www.linkedin.com/in/shielamae/">
                    <i class="fa-brands fa-linkedin"></i>
                  </a>
                </li>
                <li>
                  <a href="https://gitlab.com/shielakrabbenborg">
                    <i class="fa-brands fa-square-gitlab"></i>
                  </a>
                </li>
                <li>
                  <a href="https://github.com/shielakrabbenborg">
                    <i class="fa-brands fa-square-github"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <header>
        <Navbar expand="lg" className="Navbar sticky-top">
          <Container>
            <LinkContainer to="/">
              <Navbar.Brand>
                <img
                  src="./skinkr-logo.png"
                  alt="SKINKR Logo"
                  style={{ height: '30px' }}
                />
              </Navbar.Brand>
            </LinkContainer>

            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <SearchBox />
              <Nav className="me-auto  w-100  justify-content-end">
                <Link to="/products" className="nav-link">
                  <i className="fas fa-heart"></i> View All Products
                </Link>

                <Link to="/cart" className="nav-link">
                  <i className="fas fa-shopping-cart"></i> Cart
                  {cart.cartItems.length > 0 && (
                    <Badge pill bg="danger">
                      {cart.cartItems.reduce((a, c) => a + c.quantity, 0)}
                    </Badge>
                  )}
                </Link>

                {userInfo && (
                  <NavDropdown
                    title={
                      <>
                        <i className="fas fa-user"></i>{' '}
                        {`${userInfo.firstName} ${userInfo.lastName}`}
                      </>
                    }
                    id="#dropdown"
                  >
                    <LinkContainer to="/profile">
                      <NavDropdown.Item>User Profile</NavDropdown.Item>
                    </LinkContainer>
                    <LinkContainer to="/orderhistory">
                      <NavDropdown.Item>Order History</NavDropdown.Item>
                    </LinkContainer>
                    <NavDropdown.Divider />
                    <Link
                      className="dropdown-item"
                      to="#logout"
                      onClick={logoutHandler}
                    >
                      Logout
                    </Link>
                  </NavDropdown>
                )}

                {!userInfo && (
                  <Link
                    className="nav-link"
                    to="/login"
                    style={{
                      backgroundImage:
                        'linear-gradient(178.9deg, rgba(176, 57, 105, 1) 5.1%, rgba(229, 113, 159, 1) 109.3%)',
                      borderRadius: '5px',
                      padding: '8px 16px',
                      color: '#fff',
                    }}
                  >
                    Login
                  </Link>
                )}

                {userInfo && userInfo.isAdmin && (
                  <NavDropdown title="Admin Tools" id="admin-nav-dropdown">
                    <LinkContainer to="/admin/dashboard">
                      <NavDropdown.Item>Dashboard</NavDropdown.Item>
                    </LinkContainer>
                    <LinkContainer to="/admin/products">
                      <NavDropdown.Item>Products</NavDropdown.Item>
                    </LinkContainer>
                    <LinkContainer to="/admin/orders">
                      <NavDropdown.Item>Orders</NavDropdown.Item>
                    </LinkContainer>
                    <LinkContainer to="/admin/users">
                      <NavDropdown.Item>Users</NavDropdown.Item>
                    </LinkContainer>
                  </NavDropdown>
                )}
              </Nav>
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </header>

      <main>
        <Container className="mt-5">
          <Routes>
            <Route path="/products" element={<ProductsCataloguePage />} />
            <Route path="/product/:slug" element={<ProductPage />} />
            <Route path="/cart" element={<CartPage />} />
            <Route path="/search" element={<SearchPage />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/register" element={<RegisterPage />} />
            <Route
              path="/profile"
              element={
                <ProtectedRoute>
                  <ProfilePage />
                </ProtectedRoute>
              }
            />
            <Route path="/checkout" element={<CheckoutPage />} />

            <Route
              path="/order/:id"
              element={
                <ProtectedRoute>
                  <OrderPage />
                </ProtectedRoute>
              }
            ></Route>

            <Route
              path="/orderhistory"
              element={
                <ProtectedRoute>
                  <OrderHistoryPage />
                </ProtectedRoute>
              }
            ></Route>

            <Route path="/shipping" element={<ShippingAddressPage />}></Route>
            <Route path="/payment" element={<PaymentPage />} />
            {/* Admin Routes */}
            <Route
              path="/admin/dashboard"
              element={
                <AdminRoute>
                  <AdminDashboardPage />
                </AdminRoute>
              }
            ></Route>
            <Route
              path="/admin/orders"
              element={
                <AdminRoute>
                  <OrderListPage />
                </AdminRoute>
              }
            ></Route>
            <Route
              path="/admin/users"
              element={
                <AdminRoute>
                  <UserListPage />
                </AdminRoute>
              }
            ></Route>
            <Route
              path="/admin/products"
              element={
                <AdminRoute>
                  <ProductListPage />
                </AdminRoute>
              }
            ></Route>
            <Route
              path="/admin/product/:id"
              element={
                <AdminRoute>
                  <ProductEditPage />
                </AdminRoute>
              }
            ></Route>
            <Route
              path="/admin/user/:id"
              element={
                <AdminRoute>
                  <UserEditPage />
                </AdminRoute>
              }
            ></Route>

            <Route path="/" element={<HomePage />} />
          </Routes>
        </Container>
      </main>
      <footer className="bg-light text-dark pt-3 pb-3">
        {/* <Row className="d-flex justify-content-center">
            <Col className="text-center">
              <ul className="socials socials-list">
                <li>
                  <a href="https://www.facebook.com/shielalaland/">
                    <i className="fa-brands fa-square-facebook"></i>
                  </a>
                </li>
                <li>
                  <a href="https://www.linkedin.com/in/shielamae/">
                    <i className="fa-brands fa-linkedin"></i>
                  </a>
                </li>
                <li>
                  <a href="https://gitlab.com/shielakrabbenborg">
                    <i className="fa-brands fa-square-gitlab"></i>
                  </a>
                </li>
                <li>
                  <a href="https://github.com/shielakrabbenborg">
                    <i className="fa-brands fa-square-github"></i>
                  </a>
                </li>
              </ul>
            </Col>
          </Row> */}
        <Container className="text-center text-md-left" pt-4>
          <Row className="d-flex justify-content-center">
            <Col>
              <p>
                &copy; 2023 Copyright. Created By:&nbsp;
                <a href="mailto:hello@shielamaevs.com" className="footer-brand">
                  <strong>Shiela Mae Krabbenborg</strong>
                </a>
              </p>
            </Col>
          </Row>
        </Container>
      </footer>
    </BrowserRouter>
  );
}

export default App;
